from nlp_zero import *
import re
import pandas as pd
#import pymongo
import logging
logging.basicConfig(level = logging.INFO, format = '%(asctime)s - %(name)s - %(message)s')


class D: # 读取比赛方所给语料
    def __iter__(self):
        with open('weibo.txt') as f:
            for l in f:
                l = l.strip()#.decode('utf-8')
                l = re.sub(u'[^\u4e00-\u9fa5]+', ' ', l)
                yield l


class DO: # 读取自己的语料（相当于平行语料）
    def __iter__(self):
        with open('新建文本文档.txt') as f:
            for l in f:
                l = l.strip()#.decode('utf-8')
                l = re.sub(u'[^\u4e00-\u9fa5]+', ' ', l)
                yield l


# 在比赛方语料中做新词发现
f = Word_Finder(min_proba=1e-6, min_pmi=0.5)
f.train(D()) # 统计互信息
f.find(D()) # 构建词库

# 导出词表
words = pd.Series(f.words).sort_values(ascending=False)


# 在自己的语料中做新词发现
fo = Word_Finder(min_proba=1e-6, min_pmi=0.5)
fo.train(DO()) # 统计互信息
fo.find(DO()) # 构建词库

# 导出词表
other_words = pd.Series(fo.words).sort_values(ascending=False)
other_words = other_words / other_words.sum() * words.sum() # 总词频归一化（这样才便于对比）
print(other_words)

"""对比两份语料词频，得到特征词。
对比指标是（比赛方语料的词频 + alpha）/（自己语料的词频 + beta）；
alpha和beta的计算参考自 http://www.matrix67.com/blog/archives/5044
"""

WORDS = words.copy()
OTHER_WORDS = other_words.copy()

total_zeros = (WORDS + OTHER_WORDS).fillna(0) * 0
words = WORDS + total_zeros
other_words = OTHER_WORDS + total_zeros
total = words + other_words

alpha = words.sum() / total.sum()

result = (words + total.mean() * alpha) / (total + total.mean())
result = result.sort_values(ascending=False)
idxs = [i for i in result.index if len(i) >= 2] # 排除掉单字词

# 导出csv格式
pd.Series(idxs[:20000]).to_csv('result_1.csv', encoding='utf-8', header=None, index=None)

# nlp zero提供了良好的封装，可以直到导出一个分词器，词表是新词发现得到的词表。
tokenizer = f.export_tokenizer()

class DW:
    def __iter__(self):
        for l in D():
            yield tokenizer.tokenize(l, combine_Aa123=False)


from gensim.models import Word2Vec

word_size = 100
word2vec = Word2Vec(DW(), size=word_size, min_count=1, sg=1, negative=10)

import numpy as np
from multiprocessing.dummy import Queue


def most_similar(word, center_vec=None, neg_vec=None):
    """根据给定词、中心向量和负向量找最相近的词
    """
    vec = word2vec[word] + center_vec - neg_vec
    return word2vec.similar_by_vector(vec, topn=200)


def find_words(start_words, center_words=None, neg_words=None, min_sim=0.6, max_sim=1., alpha=0.25):
    if center_words == None and neg_words == None:
        min_sim = max(min_sim, 0.6)
    center_vec, neg_vec = np.zeros([word_size]), np.zeros([word_size])
    if center_words: # 中心向量是所有中心种子词向量的平均
        _ = 0
        for w in center_words:
            if w in word2vec.wv.vocab:
                center_vec += word2vec[w]
                _ += 1
        if _ > 0:
            center_vec /= _
    if neg_words: # 负向量是所有负种子词向量的平均（本文没有用到它）
        _ = 0
        for w in neg_words:
            if w in word2vec.wv.vocab:
                neg_vec += word2vec[w]
                _ += 1
        if _ > 0:
            neg_vec /= _
    queue_count = 1
    task_count = 0
    cluster = []
    queue = Queue() # 建立队列
    for w in start_words:
        queue.put((0, w))
        if w not in cluster:
            cluster.append(w)
    while not queue.empty():
        idx, word = queue.get()
        queue_count -= 1
        task_count += 1
        sims = most_similar(word, center_vec, neg_vec)
        min_sim_ = min_sim + (max_sim-min_sim) * (1-np.exp(-alpha*idx))
        if task_count % 10 == 0:
            log = '%s in cluster, %s in queue, %s tasks done, %s min_sim'%(len(cluster), queue_count, task_count, min_sim_)
            print(log)
        for i,j in sims:
            if j >= min_sim_:
                if i not in cluster and is_good(i): # is_good是人工写的过滤规则
                    queue.put((idx+1, i))
                    if i not in cluster and is_good(i):
                        cluster.append(i)
                    queue_count += 1
    return cluster
def is_good(w):
    if re.findall(u'[\u4e00-\u9fa5]', w) \
        and len(w) >= 2\
        and not re.findall(u'[较很越增]|[多少大小长短高低好差]', w)\
        and not u'的' in w\
        and not u'了' in w\
        and not u'这' in w\
        and not u'那' in w\
        and not u'到' in w\
        and not w[-1] in u'为一人给内中后省市局院上所在有与及厂稿下厅部商者从奖出'\
        and not w[0] in u'每各该个被其从与及当为'\
        and not w[-2:] in [u'问题', u'薯宝宝', u'他们家', u'合约', u'假设', u'编号', u'预算', u'施加', u'战略', u'状况', u'工作', u'考核', u'评估', u'需求', u'沟通', u'阶段', u'账号', u'意识', u'价值', u'事故', u'竞争', u'交易', u'趋势', u'主任', u'价格', u'门户', u'治区', u'培养', u'职责', u'社会', u'主义', u'办法', u'干部', u'员会', u'商务', u'发展', u'原因', u'情况', u'国家', u'园区', u'伙伴', u'对手', u'目标', u'委员', u'人员', u'如下', u'况下', u'见图', u'全国', u'创新', u'共享', u'资讯', u'队伍', u'农村', u'贡献', u'争力', u'地区', u'客户', u'领域', u'查询', u'应用', u'可以', u'运营', u'成员', u'书记', u'附近', u'结果', u'经理', u'学位', u'经营', u'思想', u'监管', u'能力', u'责任', u'意见', u'精神', u'讲话', u'营销', u'业务', u'总裁', u'见表', u'电力', u'主编', u'作者', u'专辑', u'学报', u'创建', u'支持', u'资助', u'规划', u'计划', u'资金', u'代表', u'部门', u'版社', u'表明', u'证明', u'专家', u'教授', u'教师', u'基金', u'如图', u'位于', u'从事', u'公司', u'企业', u'专业', u'思路', u'集团', u'建设', u'管理', u'水平', u'领导', u'体系', u'政务', u'单位', u'部分', u'董事', u'院士', u'经济', u'意义', u'内部', u'项目', u'建设', u'服务', u'总部', u'管理', u'讨论', u'改进', u'文献']\
        and not w[:2] in [u'zhuy', u'图中', u'每个', u'出席', u'一个', u'随着', u'不会', u'本次', u'产生', u'查询', u'是否', u'作者']\
        and not (u'主要是' in w or u'问题是' in w or u'可是' in w)\
        and not (len(set(w)) == 1 and len(w) > 1)\
        and not (w[0] in u'一二三四五六七八九十' and len(w) == 2)\
        and re.findall(u'[^一七厂月二夕气产兰丫田洲户尹尸甲乙日卜几口工旧门目曰石闷匕勺]', w)\
        and not u'进一步' in w:
        return True
    else:
        return False

# 种子词，在第一步得到的词表中的前面部分挑一挑即可，不需要特别准
start_words = [u'成分']
#['的' '成分' '了' '是' '成分党' '有' '护肤' '萌萌哒' '所以' '宝宝' '用' '好' '赞' '不' '的成分' '我', '面霜' '维生素' '精华' '在' '科学护肤成分党' '这' '飞吻' '皮肤' '但是' '抗氧化' '暗中观察' '和' '派对', '品牌' '分' '美白' '一' '水' '真的' '敏感肌' '这个' '醇' '烟酰胺' '买爆' '维' '可' '哇' '图' '对', '晚' '可以' '但' '黄金薯' '就是' '修丽可' '推荐' '有效' '肤质' '肌肤' '保湿' '得意' '害羞' '偷笑', '护肤品' '产品' '眼霜' '效果' '成' '传明酸' '的作用' '除了' '大' '面膜' '成分党看过来' '视黄醇' '里面', '性价比' '的产品' '功效' '成分党必看' '水乳' '分享' '因为' '科学护肤' '还有' '真' '玻尿酸' '抗衰老' '上', '的效果' '果酸' '色色' '用的' '也' '的时候' '早' '白' '以' '一样' '熊果苷' '能力' '不同' '微笑' '我的']
cluster_words = find_words(start_words, min_sim=0.6, alpha=0.35)
#('龙胆提取物', 1.3538326978753694) ('抗坏血酸', 1.3538326978753694) ('换季护肤', 1.3538326978753694) ('换季', 1.3538326978753694) ('挺好', 1.3538326978753694) ('按摩', 1.3538326978753694) ('抱着试一试', 1.3538326978753694) ('抗糖', 1.3538326978753694) ('抗痘', 1.3538326978753694) ('舒
result2 = result[cluster_words].sort_values(ascending=False)
idxs = [i for i in result2.index if is_good(i)]

pd.Series([i for i in idxs if len(i) > 2][:10000]).to_csv('result_1_2.csv', encoding='utf-8', header=None, index=None)