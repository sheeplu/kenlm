import collections
import wordcloud # 词云展示库
from PIL import Image # 图像处理库
import matplotlib.pyplot as plt # 图像展示库
import jieba
import pandas as pd
import re
import matplotlib.font_manager as fm
# 话题词频# start 3
from wordcloud import WordCloud

with open("新建文本文档.txt", 'rb') as q:
    dataT = q.read()
    dataTT = dataT.decode('utf-8')
    T = dataTT.encode('gb18030')
    text = T.decode('gb18030')

with open("cn_stopwords.txt", 'rb') as q:
    dataT = q.read()
    dataTT = dataT.decode('utf-8')
    T = dataTT.encode('gb18030')
    stopkey = T.decode('gb18030')

huati_freq=[]
jieba.load_userdict("dir.txt")#导入词表
#stopkey = [line.strip() for line in open('cn_stopwords.txt').readlines()]#导入停用词库, encoding='UTF-8'
#text=[line.strip() for line in open('新建文本文档.txt', encoding='UTF-8').readlines()]#导入文本数据
b = str(text).replace('?', '').replace('', '').replace(',', '').replace('[', '').replace(']', '').replace('/', '').replace('\\', '').replace('"', '').replace(':', '').replace('\'', '').replace('\'', '').replace('{', '').replace('}', '')
b=b.encode('UTF-8')
remove_words = stopkey
object_list = []
seg_list_exact = jieba.lcut(b)  # 搜索引擎模式
for word in seg_list_exact:  # 循环读出每个分词
    if word not in remove_words:  # 如果不在去除词库中
        object_list.append(word)  # 分词追加到列表
word_counts = collections.Counter(object_list)  # 对分词做词频统计
word_counts_top50 = word_counts.most_common(1000)  # 获取前50最高频的词
print(word_counts)  # 输出检查
with open("word_count.txt","w") as f :
    f.write(str(word_counts_top50))
huati_freq.append(word_counts_top50)
cut_text = " ".join(jieba.cut(b))
print(cut_text)

#生成
wc=WordCloud(
    background_color="white",
    max_words=500,
   # mask=bg,            #设置词云形状,或者"mask=bg,"生成指定bg图
    max_font_size=30,
    scale=8,               #越大越清楚，需要时间也越长
    random_state=20,
    font_path='Microsoft YaHei UI Light.ttf'   #中文处理，不然会乱码
    ).generate(str(object_list).replace("'", ""))
#为图片设置字体
my_font=fm.FontProperties(fname='Microsoft YaHei UI Light.ttf')
#产生背景图片，基于彩色图像的颜色生成器
#image_colors=ImageColorGenerator(bg)
#开始画图
plt.imshow(wc,interpolation="bilinear")
#为云图去掉坐标轴
plt.axis("off")
#画云图，显示
#plt.figure()
plt.show()
#为背景图去掉坐标轴
plt.axis("off")
#plt.imshow(bg,cmap=plt.cm.gray)
plt.show()

#保存云图
wc.to_file("ciyun.png")
print("词云图片已保存")